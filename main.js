// Modules to control application life and create native browser window
const {app, BrowserWindow,screen} = require('electron')
const electron = require('electron')
const path = require('path')
const menu = electron.Menu

function createWindow () {
  // Create the browser window.
  let size = screen.getPrimaryDisplay().workAreaSize
  let width = parseInt(size.width)
  let height = parseInt(size.height)

  const mainWindow = new BrowserWindow({
    //width: width,
    //eight: height,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      enableRemoteModule:true
    },
    show: false
  })
  mainWindow.maximize() 
  mainWindow.show()
  
  //menu.setApplicationMenu(null)//隐藏菜单栏
  
  // and load the index.html of the app.
  mainWindow.loadFile('index.html')//本地页面

  //mainWindow.loadURL('http://192.168.3.250:8088')//url
  
  //mainWindow.webContents.openDevTools({mode:'bottom'})//调试模式类似打开F12
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow()
  
  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
