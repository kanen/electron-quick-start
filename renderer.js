// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

//渲染进程
var argv = require('electron').remote.process.argv;
const fs = require('fs')
var file_path = argv[1];//获取被打开的文件路径
var xmlhttp;
if(!file_path || file_path =="" || file_path =="." ){
  file_path = "C:\\Users\\admin\\Desktop\\222.txt";//utf-8 格式的文件
  //file_path = "C:\\Users\\admin\\Desktop\\OFD\\999.ofd";
}

document.getElementById("file_path").innerHTML=file_path;

fs.readFile(file_path, 'utf-8', function (err, data) {
  if (err) {
      document.getElementById("file_content").innerHTML=err.message;//获取被打开的文件路径
      return
  }
  //Display the file contents
  console.log("The file content is : " + data)
  var content = data.replace(/&/g, '&amp;').replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\"/g, "&quot;").replace(/\'/g, "&#39;").replace(/\n/g, "<br/>");;
  document.getElementById("file_content").innerHTML=content;//获取被打开的文件路径
})
